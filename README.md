[![pipeline status](https://gitlab.com/kpmeen/docker-scala-sbt/badges/master/pipeline.svg)](https://gitlab.com/kpmeen/docker-scala-sbt/commits/master)

# docker-scala-sbt
Docker Image containing Scala, sbt and Zulu OpenJDK
